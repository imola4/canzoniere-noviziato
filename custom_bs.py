from glob import glob

for page in glob("songbook/*.html"):
    with open(page) as f:
        new_page = ['link rel="stylesheet" href="https://bootstrap.imola4.org/main5.css"><link rel="stylesheet" href="custom.css">' if i.find(
            "bootstrap") != -1 and i.find(".css") else i for i in f.read().split('<')]
    with open(page, 'w') as f:
        f.write('<'.join(new_page))
